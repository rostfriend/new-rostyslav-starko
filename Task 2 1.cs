﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ConsoleApplication1
{
    class Program
    {
        class Polynom
        {
            //The array of polynom coeficients
            double[] coef;
            //The degree of the polynom
            public int Degree { get; private set; }

            //Constructor1 - builds polynom from array of coefs
            public Polynom(double[] coef)
            {
                this.coef = coef;
                Degree = coef.Length - 1;
            }
            //Constructor2- builds polynom of certain degrre but without coefs
            public Polynom(int d)//     
            {
                coef = new double[d];
                Degree = coef.Length - 1;
            }

            public double this[int m]
            {
                get { return coef[m]; }
                private set { coef[m] = value; }
            }
            //Calculates the polynom by certain argument
            public double ArgumentCalc(double arg)
            {
                double result = 0;

                for (int i = 0; i <= Degree; i++)
                {
                    result += this[i] * Math.Pow(arg, i);
                }

                return result;
            }
            public static Polynom operator +(Polynom p1, Polynom p2)
            {

                int min = Math.Min(p1.Degree, p2.Degree);
                int max = Math.Max(p1.Degree, p2.Degree);

                Polynom p3 = new Polynom(max + 1);

                for (int i = 0; i < max + 1; i++)
                {
                    if (i <= min)
                    {
                        p3[i] = p1[i] + p2[i];
                    }
                    else if (p1.Degree > p2.Degree)
                    {
                        p3[i] = p1[i];
                    }
                    else
                    {
                        p3[i] = p2[i];
                    }
                }

                return p3;
            }

            public static Polynom operator -(Polynom p1, Polynom p2)
            {

                int min = Math.Min(p1.Degree, p2.Degree);
                int max = Math.Max(p1.Degree, p2.Degree);

                Polynom p3 = new Polynom(max + 1);

                for (int i = 0; i < max + 1; i++)
                {
                    if (i <= min)
                    {
                        p3[i] = p1[i] - p2[i];
                    }
                    else if (p1.Degree > p2.Degree)
                    {
                        p3[i] = p1[i];
                    }
                    else
                    {
                        p3[i] = -p2[i];
                    }
                }

                return p3;

            }

            public static Polynom Multiply(Polynom p1, Polynom p2)
            {
                Polynom p3 = new Polynom(p1.Degree + p2.Degree + 1);//!!!!!!!!!

                for (int i = 0; i <= p1.Degree; i++)
                {
                    p3 += MultElOnPolynom(p2, p1[i], i);
                }

                return p3;
            }
            //Helps to muliply polynoms
            static Polynom MultElOnPolynom(Polynom p1, double coeficient, int degree)
            {
                Polynom p2 = new Polynom(p1.Degree + 1 + degree);

                for (int i = 0; i < p1.Degree + 1; i++)
                {
                    p2[i + degree] = p1[i] * coeficient;
                }
                return p2;
            }
            public static Polynom operator *(Polynom p1, Polynom p2)
            {
                return Multiply(p1, p2);
            }


            public override string ToString()
            {
                StringBuilder st = new StringBuilder();

                for (int i = 0; i < Degree + 1; i++)
                {
                    st.Append(coef[i]);
                    st.Append("x^");
                    st.Append(i);
                    st.Append(" ");
                }
                return st.ToString();
            }
        }


        static void Main(string[] args)
        {

            Console.WriteLine("Task2: Polynoms");

            Console.Write("Lets create polynom1: ");
            Polynom pol1 = new Polynom(new double[] { 1, 3, 4, 5, 6 });
            Console.WriteLine(pol1);

            Console.Write("Lets create polynom1: ");
            Polynom pol2 = new Polynom(new double[] { 2, 4, 34, 23 });
            Console.WriteLine(pol2);
            Console.WriteLine();

            Console.WriteLine("Operation of addition of two polynoms.");
            Console.Write("Result = ");
            Polynom pol3 = pol1 + pol2;
            Console.WriteLine(pol3);
            Console.WriteLine();

            Console.WriteLine("Operation of substraction of polynoms.");
            Console.Write("Result = ");
            Polynom pol4 = pol1 - pol2;
            Console.WriteLine(pol4);
            Console.WriteLine();

            Console.WriteLine("Operation of multiplication of polynoms.");
            Console.Write("Result = ");
            Polynom pol5 = pol1 * pol2;
            Console.WriteLine(pol5);
            Console.WriteLine();

            Console.WriteLine("Operation of calculation polynom1 by certain argument");
            Console.WriteLine("Argument = 3");
            double result = pol1.ArgumentCalc(3);
            Console.WriteLine("Result = {0}", result);

            Console.ReadKey();
        }
    }
}
