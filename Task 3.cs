﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApplication1
{
    class Program
    {
        class Vector
        {
            //Array for vector components
            int[] vector;
            //The number of vector components
            public int Length { get; private set; }
            public Vector(params int[] components)
            {
                foreach (var item in components)
                {
                    if (item < 0 || item > 2)
                    {
                        throw new Exception("The coordinate values must be {0,1,2}!");
                    }
                }
                vector = components;
                Length = components.Length;
            }

            public int this[int m]
            {
                get { return vector[m]; }
                set { vector[m] = value; }
            }

            public Vector()
            {

            }
            public override string ToString()
            {
                StringBuilder str = new StringBuilder();

                foreach (var item in vector)
                {
                    str.Append(String.Format(" {0}", item));
                }
                return str.ToString();
            }
        }


        static class VectorOperations
        {
            //Scalar product of two vectors
            public static double ScalarProduct(Vector vec1, Vector vec2)
            {
                if (vec1.Length == vec2.Length)
                {
                    int prod = 0;

                    for (int i = 0; i < vec1.Length; i++)
                    {
                        prod += vec1[i] * vec2[i];
                    }
                    return prod;
                }
                else
                {
                    throw new Exception("Vectors are no of the same dimention!");
                }


            }
            //Whether two vectors are ortogonal
            public static bool AreOrtogonal(Vector vec1, Vector vec2)
            {
                return ScalarProduct(vec1, vec2) == 0;
            }
            //How many components are equal two
            public static int ComponentsEqualTwo(Vector vec)
            {
                int count = 0;

                for (int i = 0; i < vec.Length; i++)
                {
                    if (vec[i] == 2)
                    {
                        count++;
                    }
                }

                return count;
            }
            //Intersection
            public static Vector Intersection(Vector vec1, Vector vec2)
            {
                int length = vec1.Length;
                int[] vec3 = new int[length];

                if (vec1.Length == vec2.Length && !VectorOperations.AreOrtogonal(vec1, vec2))
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (vec1[i] == vec2[i])
                        {
                            switch ((vec1[i]))
                            {
                                case 0:
                                    {
                                        vec3[i] = 0;
                                        break;
                                    }
                                case 1:
                                    {
                                        vec3[i] = 1;
                                        break;
                                    }
                                case 2:
                                    {
                                        vec3[i] = 2;
                                        break;
                                    }

                            }
                        }
                        else if ((vec1[i] == 1 && vec2[i] == 2) || (vec1[i] == 2 && vec2[i] == 1))
                        {
                            vec3[i] = 1;
                        }
                        else if (vec1[i] == 0 || vec2[i] == 0)
                        {
                            vec3[i] = 0;
                        }
                        else
                        {
                            vec3[i] = 2;
                        }
                    }

                }
                else
                {
                    throw new Exception("Vectors are not ortogonal or not of the same dimention!");
                }
                return new Vector(vec3);
            }


        }
        static void Main(string[] args)
        {
            Console.WriteLine("Task 3: Multy-dimentional vector for coordinates {0,1,2}");
            Console.WriteLine("Lets create such two vectors: {1,2,0,2,1,0} and {2,0,0,1,1,2}");
            Console.WriteLine();

            Vector vect1 = new Vector(1, 2, 0, 2, 1, 0);
            Vector vect2 = new Vector(2, 0, 0, 1, 1, 2);

            Console.WriteLine("Lets check whether these vectors are ortogonal");

            if (VectorOperations.AreOrtogonal(vect1, vect2))
            {
                Console.WriteLine("Vectors are ortogonal!");
            }
            else
            {
                Console.WriteLine("Vectors are not ortogonal!");
            }
            Console.WriteLine();

            Console.WriteLine("Now lets check how many components in vector1 are equal 2");
            int comp = VectorOperations.ComponentsEqualTwo(vect1);
            Console.WriteLine("{0} component in vector1 are equal 2", comp);
            Console.WriteLine();

            Console.WriteLine("Now lets do the operation of po-elemental intersection of vectors 1 and 2");
            Console.Write("The result vector is: ");
            Console.WriteLine(VectorOperations.Intersection(vect1, vect2));

            Console.ReadKey();

        }
    }
}
