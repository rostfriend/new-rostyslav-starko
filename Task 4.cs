﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{   
    //The point
    class Point
    {
        public double X { get; private set; }
        public double Y { get; private set; }
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }

    class Rectangle
    {
        public Point A { get; private set; }
        public Point B { get; private set; }
        public Point C { get; private set; }
        public Point D { get; private set; }
        public Point Centre { get; private set; }

        //Constructor takes only the coordinates of left bottom ant top right point
        public Rectangle(double x1, double y1, double x2, double y2)
        {
            A = new Point(x1, y1);
            B = new Point(x1, y2);
            C = new Point(x2, y2);
            D = new Point(x2, y1);

            Centre = new Point((D.X - A.X) / 2, (B.Y - A.Y) / 2);
        }
        public override string ToString()
        {
            return String.Format("PointA: [{0},{1}], PointB: [{2},{3}], PointC: [{4},{5}], PointD: [{6},{7}]", A.X, A.Y, B.X, B.Y, C.X, C.Y, D.X, D.Y);
        }

    }

    static class RectangleOperation
    {   
        //Moves the centre current rectangle by its centre point to the other point
        public static Rectangle MoveCentreToPoint(Rectangle rec, double x, double y)
        {
            return new Rectangle(x - (rec.Centre.X - rec.A.X), y - (rec.Centre.Y - rec.A.Y), x + (rec.C.X - rec.Centre.X), y + (rec.C.Y - rec.Centre.Y));
        }
        //Changes size of rectangle
        public static Rectangle ChangeSize(Rectangle rec, double width, double height)
        {
            return new Rectangle(rec.A.X, rec.A.Y, rec.C.X + width, rec.C.Y + height);
        }
        //FindS the smallest rectangle enclosing first and second rectangle
        public static Rectangle SmallestEnclosesTwoRect(Rectangle rec1, Rectangle rec2)
        {
            double x1 = Math.Min(rec1.A.X, rec2.A.X);
            double y1 = Math.Min(rec1.A.Y, rec2.A.Y);
            double x2 = Math.Max(rec1.C.X, rec2.C.X);
            double y2 = Math.Max(rec1.C.Y, rec2.C.Y);

            return new Rectangle(x1, y1, x2, y2);
        }

        public static Rectangle ResultOfIntersection(Rectangle rec1, Rectangle rec2)
        {

            if (DoIntersect(rec1, rec2))
            {
                double x1 = Math.Max(rec1.A.X, rec2.A.X);
                double y1 = Math.Max(rec1.A.Y, rec2.A.Y);
                double x2 = Math.Min(rec1.C.X, rec2.C.X);
                double y2 = Math.Min(rec1.C.Y, rec2.C.Y);

                return new Rectangle(x1, y1, x2, y2);
            }
            else
            {
                throw new Exception("Rectangles dont intersect!");
            }
        }

        static bool DoIntersect(Rectangle rec1, Rectangle rec2)
        {
            if ((rec1.D.X>rec1.A.X && rec2.D.X>rec1.A.X) && (rec2.B.Y>rec1.A.Y && rec2.A.Y<rec1.B.Y))
            {
                return true;
            }
            else return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task4: Rectangles");
            Console.WriteLine("Lets create rectangle 1");
            Rectangle rect1 = new Rectangle(2, 2, 8, 8);
            Console.WriteLine(rect1);
            Console.WriteLine();

            Console.WriteLine("Lets create rectangle 2");
            Rectangle rect2 = new Rectangle(5, 5, 10, 10);
            Console.WriteLine(rect2);
            Console.WriteLine();

            
            Console.WriteLine("Lets move the rectangle 2 centre to position [3,4]");
            Console.WriteLine(RectangleOperation.MoveCentreToPoint(rect2, 3, 4));
            Console.WriteLine();

            Console.WriteLine("Changing size of the first rectangle, adding 6 points of height and 4 of width");
            Console.WriteLine(RectangleOperation.ChangeSize(rect1, 6, 4));
            Console.WriteLine();

            Console.WriteLine("Lets intersect rectangle 1 and rectangle 2");
            Console.WriteLine(RectangleOperation.ResultOfIntersection(rect1, rect2));
            Console.WriteLine();

            Console.WriteLine("Lets find the smallest rectangle enclosing rectangle 1 and rectangle 2");
            Console.WriteLine(RectangleOperation.SmallestEnclosesTwoRect(rect1, rect2));
            Console.ReadKey();
        }
    }
}
