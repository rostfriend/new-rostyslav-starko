﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApplication1
{
    class Program
    {
        public class Matrix 
        {
            public int Rows { get; private set; }
            public int Columns { get; private set; }

            double[,] matrix;

            //Costructor for creating matrix with 0 values
            public Matrix(int m, int n)
            {
                Rows = m;
                Columns = n;
                matrix = new double[m, n];

            }
            //Constructor to fill the matrix with values
            public Matrix(int m, int n, double[] arr)
            {
                Rows = m;
                Columns = n;
                int k = 0;
                matrix = new double[m, n];

                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        matrix[i, j] = arr[k++];
                    }
                }

            }

            public double this[int m, int n]
            {
                get { return matrix[m, n]; }
                set { matrix[m, n] = value; }
            }

            /// Shows whether matrix is square type
            public bool IsSquare()
            {
                return (Rows == Columns);
            }
            public bool IsEmpty()//?????????
            {
                return matrix.Length == 0;
            }

            public override string ToString()
            {
                StringBuilder st = new StringBuilder();

                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                        st.Append( String.Format("{0,5:0.0}", matrix[i, j]) + " ");
                    st.Append("\r\n");
                }
                return st.ToString();
            }
        }
        public static class MatrixMinorOperations
        {
    
            // Enter columns and rows to calculate the certain minor of the certain rate
            static public double MinorCalc(Matrix matrix, int[] coloumns, int[] rows)
            {
                if (rows.Length  != coloumns.Length)
                {
                    throw new Exception("The number of rows not equals the number of columns!");
                }

                int n = rows.Length;

                if (n > matrix.Columns - 1 || n > matrix.Rows - 1)
                {
                    throw new Exception("The number of rows and columns is out of range!");
                }

                if (coloumns.Length == 1)
                {
                    return matrix[coloumns[0], rows[0]];
                }

                Matrix matrix2 = new Matrix(n, n);//!!!Rename!!!!!!&&&&&&&
                Array.Sort(coloumns);
                Array.Sort(rows);

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < coloumns.Length; j++)
                    {
                        matrix2[i, j] = matrix[rows[i], coloumns[j]];
                    }
                }
                return Determinant(matrix2);
            }

            // Enter columns and rows to  calculate the complemential minor of the certain rate.
            // Matrix must be square type!
            static public double ComplementMinorCalc(Matrix matrix, int[] columns, int[] rows)
            {
                if (matrix.Rows != matrix.Columns)
                {
                    throw new Exception("Matrix is not square!");
                }

                int n = matrix.Rows;

                if (n > matrix.Columns  || n > matrix.Rows )
                {
                    throw new Exception("The number of rows and columns is out of range!");
                }

                int[] reqarraycol = (from m in Enumerable.Range(0, n)
                                  where !columns.Contains(m)
                                  select m).ToArray();

                int[] reqarrayrow = (from m in Enumerable.Range(0, n)
                                   where !rows.Contains(m)
                                   select m).ToArray();

                return MinorCalc(matrix, reqarraycol, reqarrayrow);
            }

            //Enter the i- and j- coordinate to calculate complemential minor. 
            //Only complemential minor could be calculated with i- and j- coordinate
            static public double ComplementMinorCalcByElem(Matrix matrix, int i, int j)
            {
                return MinorCalc(matrix, new int[1] { i }, new int[1] { j });
            }
            //Determinant calculation
            static double Determinant(Matrix matrix)
            {
                if (!matrix.IsSquare() || matrix.IsEmpty())
                { throw new Exception("Determinant could not be calculated for not-square or empty matrix"); }

                int n = matrix.Rows;
                int nm1 = n - 1;
                int kp1;
                double p;
                double det = 1;
                for (int k = 0; k < nm1; k++)
                {
                    kp1 = k + 1;
                    for (int i = kp1; i < n; i++)
                    {
                        p = matrix[i, k] / matrix[k, k];
                        for (int j = kp1; j < n; j++)
                            matrix[i, j] = matrix[i, j] - p * matrix[k, j];
                    }
                }
                for (int i = 0; i < n; i++)
                    det = det * matrix[i, i];
                return Math.Abs(det);
            }

            static public double MinorAdd(double min1, double min2)
            {
                return min1 + min2;
            }
            static public double MinorSubstract(double min1, double min2)
            {
                return min1 - min2;
            }
            static public double MinorMultiply(double min1, double min2)
            {
                return min1 * min2;
            }
            static public double MinorDivide(double min1, double min2)
            {
                return min1 / min2;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Task1: Matrix and minors ");

            Console.WriteLine("Lets create the matrics with 3 rows and 4 columns ");

            Matrix mat1 = new Matrix(3, 4, new double[] { 2.45, 4, 5, 7, 6, 8, 5, 7, 9, 4 , 6 ,8 });
            Console.WriteLine(mat1);
            Console.WriteLine();

            Console.WriteLine("Now lets calculate the minor of the second degree");
            Console.WriteLine("We will do it by choosing rows 0, 1 and coloumns 1,2");
            double res1 = MatrixMinorOperations.MinorCalc(mat1, new int[2] { 0, 1 }, new int[2] { 1, 2 });
            Console.WriteLine("The result is {0}", res1);
            Console.WriteLine();

            Console.WriteLine("Now lets calculate the complement minor by rows 1,2 and colums 0,1");
            Console.WriteLine("To do that, we should have square matrix");
            Matrix mat2 = new Matrix(3, 3, new double[] { 2, 4, 5, 7, 6, 8, 5, 7, 9 });
            Console.WriteLine(mat2);

            double res2 = MatrixMinorOperations.ComplementMinorCalc(mat2, new int[2] { 1, 2 }, new int[2] { 0, 1 });
            Console.WriteLine("The result is {0}",res2);
            Console.WriteLine();

            Console.WriteLine("Now lets calculate the complement minor of the second matrix by i - row and j - coloumn");
            Console.WriteLine("For example: i = 1, j = 2");
            double res3 = MatrixMinorOperations.ComplementMinorCalcByElem(mat2, 1, 2);
            Console.WriteLine("The result is {0}", res3);
            Console.ReadKey();

        }
    }
}

